package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Ingrese una palabra");
        Scanner in = new Scanner(System.in);
        String str2 = "";
        String str1=in.nextLine();
        int i =str1.length()-1;
        while (i!=-1)
        {
            str2 = str2+str1.charAt(i);
            i--;
        }
        if (str1.compareTo(str2)==0)
        {
            System.out.println("La palabra: " +str1+ " es un palindromo");
        }else
        {
            System.out.println("La palabra: " +str1+ " NO es un palindromo");
        }
    }
}
